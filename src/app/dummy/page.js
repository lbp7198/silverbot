"use client";
import React, { useEffect } from "react";
import { updateTitle } from "../common/functions/functions";
import { useSelector } from "react-redux";

import MainLayout from "../mainLayout/MainLayout";
import Link from "next/link";

const Dummy = () => {
  const name = useSelector((state) => state.userDetails.username);
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const user_id = useSelector((state) => state.userDetails.user_id);

  useEffect(() => {
    updateTitle("Dummy");
  }, []);

  return (
    <MainLayout>
      {isLoggedIn ? (
        <h1>
          <div>
            <h1 style={{ textAlign: "center" }}>Dummy Page</h1>
            <h2>Hello {name} , Welcome Welcome</h2>

            <br />
            <br />
            <Link href={"/dummy/test"}>Go to test</Link>
          </div>
        </h1>
      ) : (
        <h1>
          Please <Link href={"/login"}>Login</Link>{" "}
        </h1>
      )}
    </MainLayout>
  );
};

export default Dummy;
