"use client";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import { useDispatch } from "react-redux";
import axios from "axios";
import MainLayout from "../mainLayout/MainLayout";
import {
  showToastError,
  showToastSuccess,
} from "../common/functions/functions";
import { baseOfficeURL, baseRPURL, loginEndpoint } from "../common/envVars";
import { updateTitle } from "../common/functions/functions";
import { login } from "@/redux/slices/authSlice";
import { updateName, updateUserId } from "../../redux/slices/UserDetailsSlice";
import Image from "next/image";
import google from "../assets/images/google.png";
import mail from "../assets/images/mail.png";
import Link from "next/link";

const Login = () => {
  const [formData, setFormData] = useState({ email: "", password: "" });
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    updateTitle("Login");
  }, []);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleLoginClick = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(
        `${baseRPURL}${loginEndpoint}`,
        formData
      );
      const userData = response.data.User_Mst[0];
      if (!userData) {
        showToastError("No user found.");
      } else {
        const { user_full_name, user_id } = userData;
        showToastSuccess("Login Successful");
        dispatch(login());
        dispatch(updateName(user_full_name));
        dispatch(updateUserId(user_id));
        router.push("/dummy");
      }
    } catch (err) {
      showToastError("Login Failed");
    }
  };

  return (
    <MainLayout>
      <section className="RegForm">
        <div className="container-xl">
          <div className="RegFormInner loginPage">
            <div className="RegHeading">
              <h2>Login</h2>
            </div>
            <div className="mainRegForm">
              <form onSubmit={handleLoginClick}>
                <div className="formInner">
                  <div className="row">
                    <div className="col-lg-12 mb-3">
                      <label htmlFor="loginemial">Email address</label>
                      <input
                        type="email"
                        id="loginemial"
                        placeholder="Enter Email Address"
                        value={formData.email}
                        name="email"
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-lg-12 mb-3">
                      <label htmlFor="loginpassword">Password</label>
                      <input
                        type="password"
                        id="loginpassword"
                        placeholder="Enter Password"
                        value={formData.password}
                        name="password"
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-lg-12 mb-3">
                      <div className="rememberDiv">
                        <div className="remember">
                          <input
                            type="checkbox"
                            name="remember"
                            id="remember"
                          />
                          <label htmlFor="remember">Remember Me</label>
                        </div>
                        <div className="forgot">
                          <Link href="/">Forgot your password?&nbsp;</Link>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-12 mb-4 d-flex justify-content-center">
                      <input
                        type="submit"
                        value="Log In"
                        className="oranageBtn logInBtn  m-auto"
                      />
                    </div>
                    <div className="col-lg-12">
                      <div className="orLine"></div>
                    </div>
                    <div className="col-lg-12">
                      <div className="loginWith">
                        <span>Login with</span>
                        <div className="loginOption">
                          <Link href="/">
                            <Image src={google} alt="google" />
                          </Link>
                          <Link href="/">
                            <Image src={mail} alt="mail" />
                          </Link>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-12 d-flex justify-content-center">
                      <p className="newAccont">
                        Don't have an account?
                        <Link href="/register">Register Now</Link>
                      </p>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </MainLayout>
  );
};

export default Login;
