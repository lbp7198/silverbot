import Link from "next/link";
import React from "react";
import InitialPage from "./initialPage/InitialPage";

export default function Home() {
  return (
    <main>
      <InitialPage />
    </main>
  );
}
