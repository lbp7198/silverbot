import React from "react";
import Navbar from "../common/navbar/Navbar";
import Footer from "../common/footer/Footer";
import ChatBot from "../common/chatbot/ChatBot";

const MainLayout = ({ children }) => {
  return (
    <div
      style={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}
    >
      <Navbar />
      <div style={{ flex: 1 }}>{children}</div>
      <Footer />
      <ChatBot />
    </div>
  );
};

export default MainLayout;
