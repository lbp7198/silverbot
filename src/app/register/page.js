"use client";
import "../assets/css/style.css";
import "../assets/css/responsive.css";
import Link from "next/link";
import { useEffect, useState } from "react";
import {
  showToastSuccess,
  showToastError,
} from "../common/functions/functions";
import { updateTitle } from "../common/functions/functions";
import { useRouter } from "next/navigation";
import MainLayout from "../mainLayout/MainLayout";
import { validatePassword } from "../common/functions/functions";
import axios from "axios";
import { baseOfficeURL, baseRPURL, registerEndpoint } from "../common/envVars";

const Register = () => {
  const [formData, setFormData] = useState({
    fullName: "",
    companyName: "",
    email: "",
    mobileNumber: "",
    password: "",
    confirmPassword: "",
  });

  const router = useRouter();

  useEffect(() => {
    updateTitle("Register");
  }, []);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      contact_no: formData.mobileNumber,
      email: formData.email,
      user_full_name: formData.fullName,
      user_password: formData.password,
      company_name: formData.companyName,
    };

    const passwordError = validatePassword(formData.password);
    if (passwordError) {
      showToastError(passwordError);
      return;
    }

    try {
      const response = await axios.post(
        `${baseRPURL}${registerEndpoint}`,
        data
      );
      console.log("Registration successful:", response.data);
      showToastSuccess("Registered Successfully");
      router.push("/login");
    } catch (error) {
      console.error("Registration failed:", error);
      showToastError("Registration failed. Please try again.");
    }
  };

  return (
    <MainLayout>
      <section className="RegForm">
        <div className="container-xl">
          <div className="RegFormInner">
            <div className="RegHeading">
              <h2>Registration</h2>
            </div>
            <div className="mainRegForm">
              <form onSubmit={handleSubmit} data-testid="registration-form">
                <div className="formInner">
                  <div className="row">
                    <div className="col-lg-6 mb-3">
                      <label htmlFor="name">Name</label>
                      <input
                        type="text"
                        id="name"
                        name="fullName"
                        placeholder="Your Full Name"
                        value={formData.fullName}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-lg-6 mb-3">
                      <label htmlFor="cname">Company Name</label>
                      <input
                        type="text"
                        id="cname"
                        name="companyName"
                        placeholder="Your Company Name"
                        value={formData.companyName}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-lg-6 mb-3">
                      <label htmlFor="Regemail">Email</label>
                      <input
                        type="email"
                        id="Regemail"
                        name="email"
                        placeholder="Your Email Address"
                        value={formData.email}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-lg-6 mb-3">
                      <label htmlFor="phone">Mobile Number</label>
                      <input
                        type="tel"
                        id="phone"
                        name="mobileNumber"
                        placeholder="Your Mobile Number"
                        value={formData.mobileNumber}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-lg-6 mb-3">
                      <label htmlFor="Regpassword">Password</label>
                      <input
                        type="password"
                        id="Regpassword"
                        name="password"
                        placeholder="Enter Password"
                        value={formData.password}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-lg-6">
                      <label htmlFor="cpassword">Confirm Password</label>
                      <input
                        type="password"
                        id="cpassword"
                        name="confirmPassword"
                        placeholder="Enter Confirm Password"
                        value={formData.confirmPassword}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div className="col-lg-12 d-flex justify-content-center">
                      <input
                        type="submit"
                        value="Register Now"
                        className="oranageBtn submitBtn  m-auto"
                      />
                    </div>

                    <div className="col-lg-12 d-flex justify-content-center">
                      <p className="newAccont">
                        Do you have an account?
                        <Link href="/login">Login Now</Link>
                      </p>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </MainLayout>
  );
};

export default Register;
