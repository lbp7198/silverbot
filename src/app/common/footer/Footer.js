"use client";
import React from "react";
import "../../assets/css/style.css";
import "../../assets/css/responsive.css";

const Footer = () => {
  return (
    <footer>
      <div className="container-xl">
        <div className="footerInner">
          <div className="row d-flex align-items-center">
            <div className="col-lg-8 col-md-12">
              <div className="footerLeft">
                <div className="footerUl">
                  <ul>
                    <li>
                      <a href="#">Terms & Conditions</a>
                    </li>
                    <li>
                      <a href="#">Privacy Policy</a>
                    </li>
                    <li>
                      <a href="#">Copyright Policy</a>
                    </li>
                    <li>
                      <a href="#">Hyperlinking Policy</a>
                    </li>
                    <li>
                      <a href="#">Disclaimer</a>
                    </li>
                    <li>
                      <a href="#">Suppot</a>
                    </li>
                  </ul>
                </div>
                <div className="footerCopyRight">
                  <p>
                    Copyright © 2023 Silver ChatBOT, Powered by Silver Touch.
                    All Rights Reserved.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-12">
              <div className="socialPart d-flex justify-content-lg-end align-content-center justify-content-center">
                <p>Social Connections</p>
                <div className="socialUl">
                  <ul>
                    <li>
                      <a href="#" title="twitter">
                        <i className="fa-brands fa-twitter"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="facebook">
                        <i className="fa-brands fa-facebook-f"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="youtube">
                        <i className="fa-brands fa-youtube"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="instagram">
                        <i className="fa-brands fa-instagram"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="linkedin">
                        <i className="fa-brands fa-linkedin"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
