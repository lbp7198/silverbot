"use client";
import React from "react";
import "../../assets/css/style.css";  
import "../../assets/css/responsive.css";
import Link from "next/link";
import Image from "next/image";
import logoImage from "../../assets/images/logo.png";

const Navbar = () => {
  return (
    <header>
      <div className="container-xl">
        <div className="headerInner">
          <nav className="navbar navbar-expand-lg navbar-light">
            <div className="container-xl">
              <h1 className="logo" title="silverChatBot">
                <Link className="navbar-brand" href="#">
                  <Image src={logoImage} alt="logo" className="img-fluid" />
                </Link>
              </h1>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="offcanvas"
                data-bs-target="#offcanvasNavbar"
                aria-controls="offcanvasNavbar"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div
                className="offcanvas offcanvas-end d-flex justify-content-lg-end flex-lg-row"
                tabIndex="-1"
                id="offcanvasNavbar"
                aria-labelledby="offcanvasNavbarLabel"
              >
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <Link className="nav-link" aria-current="page" href="/">
                      Home
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" href="#">
                      Product
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" href="#">
                      Affiliate
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" href="#">
                      Pricing
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" href="#">
                      Guide
                    </Link>
                  </li>
                </ul>
                <div className="BtnGrp">
                  <div className="NavBtn RegBtn">
                    <Link href="/register">
                      Registration
                      <i className="fa-solid fa-arrow-right-long"></i>
                    </Link>
                  </div>
                  <div className="NavBtn LoginBtn oranageBtn">
                    <Link href="/login">
                      Login <i className="fa-solid fa-arrow-right-long"></i>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </header>
  );
};

export default Navbar;
