import { toast } from "react-toastify";

//toast message functions

export const showToastSuccess = (message) => {
  toast.success(message);
};

export const showToastError = (message) => {
  toast.error(message);
};

// updateTitle function

export const updateTitle = (title) => {
  document.title = title;
};

// passwordValidation function

export const validatePassword = (password) => {
  const lengthRegex = /^.{8,}$/;
  const uppercaseRegex = /[A-Z]/;
  const digitRegex = /\d/;
  const specialCharRegex = /[@$!%*?&]/;

  if (!lengthRegex.test(password)) {
    return "Password must be at least 8 characters long.";
  } else if (!uppercaseRegex.test(password)) {
    return "Password must contain at least one uppercase letter.";
  } else if (!digitRegex.test(password)) {
    return "Password must contain at least one numeric digit.";
  } else if (!specialCharRegex.test(password)) {
    return "Password must contain at least one special character (@, $, !, %, *, ?, or &).";
  }
  return null; // No error
};
