import Image from "next/image";
import React from "react";
import chatbot from "../../assets/images/chatBot.png";

const ChatBot = () => {
  return (
    <div className="botIcon">
      <a href="#">
        <Image src={chatbot} alt="chatBot" />
      </a>
    </div>
  );
};

export default ChatBot;
