import React from "react";
import MainLayout from "../mainLayout/MainLayout";

const InitialPage = () => {
  return (
    <MainLayout>
      <div
        style={{
          // display: "flex",
          alignContent: "center",
          justifyContent: "center",
          marginTop: "100px",
        }}
      >
        <h1 style={{ textAlign: "center" }}>Initial Page</h1>

        <p style={{ margin: "10px 70px", textAlign: "center" }}>
          Warning: Reference Only This page is for reference purposes only and
          is not included in the main design.
          <br /> Please disregard any content or elements on this page as they
          are not part of the final design. Thank you for your understanding.
        </p>
      </div>
    </MainLayout>
  );
};

export default InitialPage;
