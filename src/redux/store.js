import { configureStore } from "@reduxjs/toolkit";

import { userSliceReducer } from "./slices/UserDetailsSlice";
import { authSliceReducer } from "./slices/authSlice";

const store = configureStore({
  reducer: {
    userDetails: userSliceReducer,
    auth: authSliceReducer,
  },
});

export default store;
