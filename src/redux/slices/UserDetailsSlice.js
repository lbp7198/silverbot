import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "userDetails",
  initialState: {
    username: "",
    user_id: "",
  },
  reducers: {
    updateName: (state, action) => {
      state.username = action.payload;
    },
    updateUserId: (state, action) => {
      state.user_id = action.payload;
    },
  },
});

export const { updateName, updateUserId } = userSlice.actions;
export const userSliceReducer = userSlice.reducer;
