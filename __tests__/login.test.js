import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Login from "../src/app/login/page";
import { toast } from "react-toastify";

jest.mock("react-toastify", () => ({
  toast: {
    success: jest.fn(),
    error: jest.fn(),
  },
}));

describe("Login Component", () => {
  it("renders without crashing", () => {
    render(<Login />);
  });

  it("initializes email and password fields correctly", () => {
    render(<Login />);
    const emailInput = screen.getByPlaceholderText("Enter Email Address");
    const passwordInput = screen.getByPlaceholderText("Enter Password");
    expect(emailInput).toHaveValue("");
    expect(passwordInput).toHaveValue("");
  });

  it("updates state variables on input change", () => {
    render(<Login />);
    const emailInput = screen.getByPlaceholderText("Enter Email Address");
    const passwordInput = screen.getByPlaceholderText("Enter Password");

    fireEvent.change(emailInput, { target: { value: "test@example.com" } });
    fireEvent.change(passwordInput, { target: { value: "testpassword" } });

    expect(emailInput).toHaveValue("test@example.com");
    expect(passwordInput).toHaveValue("testpassword");
  });

  it("handles form submission and displays success message", () => {
    render(<Login />);
    const emailInput = screen.getByPlaceholderText("Enter Email Address");
    const passwordInput = screen.getByPlaceholderText("Enter Password");
    const submitButton = screen.getByText("Log In");

    fireEvent.change(emailInput, { target: { value: "lucky@gmail.com" } });
    fireEvent.change(passwordInput, { target: { value: "1234" } });
    fireEvent.click(submitButton);

    // Check if toast.success was called with the expected message
    expect(toast.success).toHaveBeenCalledWith("Login Successfully");
  });

  it("handles form submission and displays error message", () => {
    render(<Login />);
    const emailInput = screen.getByPlaceholderText("Enter Email Address");
    const passwordInput = screen.getByPlaceholderText("Enter Password");
    const submitButton = screen.getByText("Log In");

    fireEvent.change(emailInput, { target: { value: "invalid@gmail.com" } });
    fireEvent.change(passwordInput, { target: { value: "invalidpassword" } });
    fireEvent.click(submitButton);

    // Check if toast.error was called with the expected message
    expect(toast.error).toHaveBeenCalledWith("Login Failed");
  });

  it("navigates to the 'Forgot Password' page when the link is clicked", () => {
    render(<Login />);
    const forgotPasswordLink = screen.getByText("Forgot your password?");
    fireEvent.click(forgotPasswordLink);

    // Add assertions here to check if navigation occurred correctly.
    // You may need to use routing testing tools or mocks for this.
  });

  it("navigates to the 'Register' page when the link is clicked", () => {
    render(<Login />);
    const registerLink = screen.getByText("Register Now");
    fireEvent.click(registerLink);

    // Add assertions here to check if navigation occurred correctly.
    // You may need to use routing testing tools or mocks for this.
  });
});
