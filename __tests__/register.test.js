import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Register from "../src/app/register/page";
import { toast } from "react-toastify";
import * as RegisterModule from "../src/app/register/page"; // Import the module

jest.mock("react-toastify", () => ({
  toast: {
    success: jest.fn(),
  },
}));

describe("Register Component", () => {
  it("renders without crashing", () => {
    render(<Register />);
  });

  it("initializes form fields correctly", () => {
    render(<Register />);
    const nameInput = screen.getByPlaceholderText("Your Full Name");
    const companyNameInput = screen.getByPlaceholderText("Your Company Name");
    const emailInput = screen.getByPlaceholderText("Your Email Address");
    const mobileNumberInput = screen.getByPlaceholderText("Your Mobile Number");
    const passwordInput = screen.getByPlaceholderText("Enter Password");
    const confirmPasswordInput = screen.getByPlaceholderText(
      "Enter Confirm Password"
    );

    expect(nameInput).toHaveValue("");
    expect(companyNameInput).toHaveValue("");
    expect(emailInput).toHaveValue("");
    expect(mobileNumberInput).toHaveValue("");
    expect(passwordInput).toHaveValue("");
    expect(confirmPasswordInput).toHaveValue("");
  });

  it("updates state variables on input change", () => {
    render(<Register />);
    const nameInput = screen.getByPlaceholderText("Your Full Name");
    const companyNameInput = screen.getByPlaceholderText("Your Company Name");
    const emailInput = screen.getByPlaceholderText("Your Email Address");
    const mobileNumberInput = screen.getByPlaceholderText("Your Mobile Number");
    const passwordInput = screen.getByPlaceholderText("Enter Password");
    const confirmPasswordInput = screen.getByPlaceholderText(
      "Enter Confirm Password"
    );

    fireEvent.change(nameInput, { target: { value: "John Doe" } });
    fireEvent.change(companyNameInput, { target: { value: "Company Inc." } });
    fireEvent.change(emailInput, { target: { value: "test@example.com" } });
    fireEvent.change(mobileNumberInput, { target: { value: "1234567890" } });
    fireEvent.change(passwordInput, { target: { value: "testpassword" } });
    fireEvent.change(confirmPasswordInput, {
      target: { value: "testpassword" },
    });

    expect(nameInput).toHaveValue("John Doe");
    expect(companyNameInput).toHaveValue("Company Inc.");
    expect(emailInput).toHaveValue("test@example.com");
    expect(mobileNumberInput).toHaveValue("1234567890");
    expect(passwordInput).toHaveValue("testpassword");
    expect(confirmPasswordInput).toHaveValue("testpassword");
  });

  //   it("should call handleSubmit when the registration form is submitted with a valid email address", () => {
  //     const { container } = render(<Register />);
  //     const form = screen.getByTestId("registration-form");
  //     const handleSubmitSpy = jest.spyOn(Register.prototype, "handleSubmit");
  //     const preventDefaultSpy = jest.spyOn(form, "preventDefault");
  //     const showAlertSpy = jest.spyOn(window, "alert");

  //     fireEvent.change(screen.getByPlaceholderText("Your Full Name"), {
  //       target: { value: "John Doe" },
  //     });
  //     fireEvent.change(screen.getByPlaceholderText("Your Company Name"), {
  //       target: { value: "ABC Company" },
  //     });
  //     fireEvent.change(screen.getByPlaceholderText("Your Email Address"), {
  //       target: { value: "test@example.com" },
  //     });
  //     fireEvent.change(screen.getByPlaceholderText("Your Mobile Number"), {
  //       target: { value: "1234567890" },
  //     });
  //     fireEvent.change(screen.getByPlaceholderText("Enter Password"), {
  //       target: { value: "password" },
  //     });
  //     fireEvent.change(screen.getByPlaceholderText("Enter Confirm Password"), {
  //       target: { value: "password" },
  //     });

  //     fireEvent.submit(form);

  //     expect(preventDefaultSpy).toHaveBeenCalled();
  //     expect(handleSubmitSpy).toHaveBeenCalled();
  //     expect(showAlertSpy).not.toHaveBeenCalled();
  //   });
  // Add more test cases for form validation and navigation as needed.
});
