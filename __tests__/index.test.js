import { render, screen } from "@testing-library/react";
import Home from "../src/app/page";
import "@testing-library/jest-dom";
import React from "react";

describe("Home", () => {
  it("renders the main heading", () => {
    render(<Home />);

    const heading = screen.getByRole("heading", {
      name: /Entry Page/i,
    });

    expect(heading).toBeInTheDocument();
  });

  it("renders the 'Login' link", () => {
    render(<Home />);

    const loginLink = screen.getByRole("link", {
      name: /Login/i,
    });

    expect(loginLink).toBeInTheDocument();
  });

  it("renders the 'Registration' link", () => {
    render(<Home />);

    const registrationLink = screen.getByRole("link", {
      name: /Registration/i,
    });

    expect(registrationLink).toBeInTheDocument();
  });

  it("renders a warning message", () => {
    render(<Home />);

    const warningMessage = screen.getByText(/Warning: Reference Only/i);

    expect(warningMessage).toBeInTheDocument();
  });

  it("renders a paragraph with the warning content", () => {
    render(<Home />);

    const warningContent = screen.getByText(
      "This page is for reference purposes only and is not included in the main design. Please disregard any content or elements on this page as they are not part of the final design. Thank you for your understanding."
    );

    expect(warningContent).toBeInTheDocument();
  });
});
