// jest.config.js
const nextJest = require("next/jest.js");

const createJestConfig = nextJest({
  dir: "./",
});

const config = {
  setupFilesAfterEnv: ["<rootDir>/jest.setup.js"],
  testEnvironment: "jest-environment-jsdom",
  moduleNameMapper: {
    // "^@/(.*)$": "<rootDir>/src/$1",
  },
  transformIgnorePatterns: ["/node_modules/", "\\.css$"],
  transform: {
    "^.+\\.[jt]sx?$": "babel-jest",
    ".js": ["jest-esm-transformer"],
  },
};

module.exports = async () => {
  const nextConfig = await createJestConfig();
  return {
    ...nextConfig,
    ...config,
  };
};

module.exports = createJestConfig(config);

// const nextJest = require('next/jest');
// const { transform } = require("@babel/core");

// const createJestConfig = nextJest({
//   // Provide the path to your Next.js app to load next.config.js and .env files in your test environment
//   dir: './',
// })

// // Add any custom config to be passed to Jest
// const customJestConfig = {
//   setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
//   testEnvironment: 'jest-environment-jsdom',
// }

// // createJestConfig is exported this way to ensure that next/jest can load the Next.js config which is async
// module.exports = createJestConfig(customJestConfig)
