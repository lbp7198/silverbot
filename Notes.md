1. Changed stylings in Style.css
   .RegForm {
   display: flex;
   align-items: center;
   height: 100%;
   margin-top: 50px; /_ dev-l changed _/
   margin-bottom: 50px; /_ dev-l changed _/
   }

2. Disabled font-awesome as it does not support child->parent tags

solved above error by importing Script from Next

// {
// "presets": ["@babel/preset-env","@babel/preset-react"],
// "plugins": ["@babel/plugin-transform-modules-commonjs"]
// }

3. handleSubmit function in register page with queries and mutations

// const handleSubmit = async (e) => {
// e.preventDefault();

// // const passwordError = validatePassword(password);

// // if (passwordError) {
// // showToastError(passwordError);
// // } else
// if (password !== confirmPassword) {
// showToastError("Password and Confirm Password do not match.");
// } else {
// const data = {
// fullName,
// companyName,
// email,
// mobileNumber,
// password,
// };

// console.log("data", data);
// try {
// const headers = {
// "x-hasura-admin-secret": "Silver_chatbot@llmproject",
// "Content-Type": "application/json",
// };

// const query = ` //         mutation insert_user(
  //           $contact_no: String!
  //           $email: String!
  //           $user_full_name: String!
  //           $user_password: String!
  //           $company_name: String!
  //         ) {
  //           insert_User_Mst(
  //             objects: {
  //               user_full_name: $user_full_name
  //               user_password: $user_password
  //               email: $email
  //               contact_no: $contact_no
  //               company_name: $company_name
  //             }
  //           ) {
  //             returning {
  //               user_id
  //               created_at
  //             }
  //           }
  //         }
  //     `;

// const variables = {
// contact_no: mobileNumber,
// email: email,
// user_full_name: fullName,
// user_password: password,
// company_name: companyName,
// };

// const response = await axios.post(
// "http://10.0.167.105:8080/api/rest/register-user",
// {
// query: query,
// variables: variables,
// },
// { headers: headers }
// );

// console.log("response", response.data);

// if (response.status === 200) {
// showToastSuccess("Registered Successfully");
// router.push("/login");
// } else {
// showToastError("Registration failed. Please try again.");
// }
// } catch (error) {
// console.log("error", error);
// showToastError("Registration failed. Please try again.");
// }
// }
// };

4. Show/Hide Password Code
   import React, { useEffect, useState } from "react";
   import Image from "next/image";
   import google from "../assets/images/google.png";
   import mail from "../assets/images/mail.png";
   import Link from "next/link";
   import { showToastError, showToastSuccess } from "../common/functions/functions";
   import { useRouter } from "next/navigation";
   import { updateTitle } from "../common/functions/functions";
   import { useDispatch } from "react-redux";
   import { updateName } from "../../redux/slices/UserDetailsSlice";
   import { login } from "@/redux/slices/authSlice";
   import MainLayout from "../mainLayout/MainLayout";
   import axios from "axios";
   import { baseOfficeURL, loginEndpoint } from "../common/config";
   import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";

const Login = () => {
const [formData, setFormData] = useState({
email: "",
password: "",
});
const [showPassword, setShowPassword] = useState(false);
const router = useRouter();
const dispatch = useDispatch();

useEffect(() => {
updateTitle("Login");
}, []);

const handleInputChange = (e) => {
const { name, value } = e.target;
setFormData({
...formData,
[name]: value,
});
};

const handleLoginClick = async (e) => {
e.preventDefault();

    try {
      const response = await axios.post(
        `${baseOfficeURL}${loginEndpoint}`,
        formData
      );

      if (
        Array.isArray(response.data.User_Mst) &&
        response.data.User_Mst.length === 0
      ) {
        showToastError("No user found.");
      } else {
        const userName = response.data.User_Mst[0].user_full_name;
        showToastSuccess("Login Successful");
        dispatch(login());
        dispatch(updateName(userName));
        router.push("/dummy");
      }
    } catch (err) {
      showToastError("Login Failed");
    }

};

return (
<MainLayout>
<section className="RegForm">
<div className="container-xl">
<div className="RegFormInner loginPage">
<div className="RegHeading">
<h2>Login</h2>
</div>
<div className="mainRegForm">
<form onSubmit={handleLoginClick}>
<div className="formInner">
<div className="row">
<div className="col-lg-12 mb-3">
<label htmlFor="loginemial">Email address</label>
<input
                        type="email"
                        id="loginemial"
                        placeholder="Enter Email Address"
                        value={formData.email}
                        name="email"
                        onChange={handleInputChange}
                        required
                      />
</div>
<div className="col-lg-12 mb-3">
<label htmlFor="loginpassword">Password</label>
<div className="password-input">
<input
type={showPassword ? "text" : "password"}
id="loginpassword"
placeholder="Enter Password"
value={formData.password}
name="password"
onChange={handleInputChange}
required
/>
<button
type="button"
className="toggle-password-button"
onClick={() => setShowPassword(!showPassword)} >
{showPassword ? (
<AiOutlineEyeInvisible />
) : (
<AiOutlineEye />
)}
</button>
</div>
</div>
<div className="col-lg-12 mb-3">
<div className="rememberDiv">
<div className="remember">
<input
                            type="checkbox"
                            name="remember"
                            id="remember"
                          />
<label htmlFor="remember">Remember Me</label>
</div>
<div className="forgot">
<Link href="/">Forgot your password?&nbsp;</Link>
</div>
</div>
</div>
<div className="col-lg-12 mb-4 d-flex justify-content-center">
<input
                        type="submit"
                        value="Log In"
                        className="oranageBtn logInBtn  m-auto"
                      />
</div>
<div className="col-lg-12">
<div className="orLine"></div>
</div>
<div className="col-lg-12">
<div className="loginWith">
<span>Login with</span>
<div className="loginOption">
<Link href="/">
<Image src={google} alt="google" />
</Link>
<Link href="/">
<Image src={mail} alt="mail" />
</Link>
</div>
</div>
</div>
<div className="col-lg-12 d-flex justify-content-center">
<p className="newAccont">
Don't have an account?
<Link href="/register">Register Now</Link>
</p>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</section>
</MainLayout>
);
};

export default Login;
